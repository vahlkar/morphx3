#include <iostream>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#define WIDTH 500
#define HEIGHT 500
#define NB_IMG_DEFAUT 5
#define FICHIER1 "img/pierre_1.jpg"
#define FICHIER2 "img/volodia_2.jpg"

#include "tableaux.h"

#define MH3_TITRE "MorpHX3_"

struct Pointeur //Structure gérant la position du pointeur à l'écran
{
	int i; //Indice du sommet en cours d'édition
	int x;
	int y;
	bool actif; //Pour deplacer un point
	bool nouveau; //Pour ajouter un point
	bool selection; //Pour identifier un point existant
	Objectif objectif;
};

static void onMouse(int event, int x, int y, int flags, void* data)
{
    /*
    	EVENT_MOUSEMOVE
		EVENT_LBUTTONDOWN
		EVENT_RBUTTONDOWN
		EVENT_MBUTTONDOWN
		EVENT_LBUTTONUP
		EVENT_RBUTTONUP
		EVENT_MBUTTONUP
		EVENT_LBUTTONDBLCLK
		EVENT_RBUTTONDBLCLK
		EVENT_MBUTTONDBLCLK
    */

    Pointeur *pointeur = (Pointeur*)data;

    switch(event)
    {
		case EVENT_LBUTTONDOWN:
			//L'actualisation des coordonnées doit se faire avant la récupération de l'indice du sommet !
			pointeur->x = x;
			pointeur->y = y;
			#ifdef OS_WINDOWS
				//Windows
				pointeur->selection = (flags == EVENT_FLAG_LBUTTON + EVENT_FLAG_SHIFTKEY);
			#else
				//Unix machine
				pointeur->selection = (flags == 48); //Valeur récupérée par test sur machine ; à adapter selon configuration clavier ?
			#endif

			//Le clic gauche permet de selectionner mais aussi de relacher un point
			if(!pointeur->actif && !pointeur->selection) pointeur->nouveau = true;
			pointeur->actif = true - (pointeur->actif); //Inversion du statut
			break;

		case EVENT_RBUTTONDOWN:
			//Le clic droit permet toujours de relacher le point sélectionné
			pointeur->actif = false;
			break;

		case EVENT_MOUSEMOVE:
			pointeur->x = x;
			pointeur->y = y;
			break;
    }
}

static void onMouse_O(int event, int x, int y, int flags, void* data)
{
	Pointeur *pointeur = (Pointeur*)data;
	if(pointeur->objectif != RENDU)
	{
		pointeur->objectif = ORIGINE;
		onMouse(event, x, y, flags, data);
	}
}
static void onMouse_D(int event, int x, int y, int flags, void* data)
{
	Pointeur *pointeur = (Pointeur*)data;
	if(pointeur->objectif != RENDU)
	{
		pointeur->objectif = DESTINATION;
		onMouse(event, x, y, flags, data);
	}
}

int main()
{
    std::cout << "MorpHX3 !\n";
    unsigned int nb_img_rendu = NB_IMG_DEFAUT;
    unsigned int frame_rate = FPS_DEFAUT;
    float cible = 0;
    std::cout << "Veuillez entrer le nombre d'image a generer : ";
    std::cin >> nb_img_rendu;
     std::cout << "Veuillez entrer le taux d'images par secondes de la video (5 par defaut) : ";
    std::cin >> frame_rate;
     std::cout << "Veuillez entrer le pourcentage (entre 0 et 1 : ex 0.5) : ";
    std::cin >> cible;


    std::string fichier1 = FICHIER1;
    std::string fichier2 = FICHIER2;

	std::string titre_O = MH3_TITRE + extraireNomFichier(fichier1);
    std::string titre_D = MH3_TITRE + extraireNomFichier(fichier2);

    Pointeur pointeur;
    pointeur.actif = false;
    pointeur.nouveau = false;
    pointeur.selection = false;
    pointeur.i = 0;
    pointeur.objectif = ORIGINE;

    Mat image1 = imread(fichier1);
    Mat image2 = imread(fichier2);
	Mat img_origine;
	Mat img_destination;

	resize(image1, img_origine, Size(WIDTH, HEIGHT),0,0);
	resize(image2, img_destination, Size(WIDTH, HEIGHT),0,0);

	Rect rect(0,0, WIDTH, HEIGHT);

	Mat ecran(rect.size(), CV_8UC3);
    ecran = Scalar::all(0);

	Tableaux monTableau(rect, img_origine, img_destination, fichier1, fichier2, frame_rate);

    namedWindow(titre_O, WINDOW_AUTOSIZE);
    	imshow(titre_O, ecran);
    namedWindow(titre_D, WINDOW_AUTOSIZE);
		imshow(titre_D, ecran);

	setMouseCallback(titre_O, onMouse_O, &pointeur);
	setMouseCallback(titre_D, onMouse_D, &pointeur);

	help();

	bool continuer = true;

	while(continuer)
	{
		int touche = waitKey(10);

		if (touche != -1)
		{
			//std::cout << "Touche : " << (char) touche << " ; n*" << touche << "\n";
			//Desactivation de toute action en cours de la souris
			pointeur.nouveau = false;
			pointeur.actif = false;
			pointeur.selection = false;

			#ifdef OS_WINDOWS
				//Windows
				switch(touche)
			#else
				//Unix machine
				switch((char)touche)
			#endif
			{
				case 27:
					continuer = false;
					break;

				case 'h':
					help();
					break;

				case 't':
					unsigned int tDebut, tFin;
					if(monTableau.verifierTailles(tDebut, tFin))
						std::cout << "Tailles identiques : " << tDebut << "\n";
					else
					{
						std::cout << "Nombre total de sommets " << fichier1 << " : " << tDebut << "\n";
						std::cout << "Nombre total de sommets " << fichier2 << " : " << tFin << "\n";
					}
					break;

				case 'r':
					//Vidage des tableaux ; pour les deux images
					monTableau.reinitialiser(pointeur.objectif);
					break;

				case 'g':
					//Affichage du maillage en cours
					monTableau.getSommets(pointeur.objectif);
					break;

				case 's':
					//Sauvegarde du maillage en cours
					monTableau.sauvegarder(pointeur.objectif);
					break;

				case 'c':
					//Chargement du maillage sauvegardé sur l'image en cours
					monTableau.charger(pointeur.objectif);
					break;

				case 'v':
					//Copie du maillage de l'autre image sur le maillage en cours
					if(pointeur.objectif == ORIGINE) monTableau.copieMaillage(DESTINATION, ORIGINE);
					else if(pointeur.objectif == DESTINATION) monTableau.copieMaillage(ORIGINE, DESTINATION);
					break;

                case 'm':
                	//Generation de la video en fonction des images pre-generees
                    monTableau.finaliserVideo(nb_img_rendu);
                    break;

                case 'i':
                	//Generation de l'image cible uniquement
                	monTableau.genererCible(cible);
                    break;

				case 'w':
					std::cout << "Point d'indice : " << monTableau.getEdition() << "\n";
					break;

				case 'p':
					//Insertion d'un point a l'indice du point en cours d'edition
					if(pointeur.objectif == ORIGINE || pointeur.objectif == DESTINATION)
					{
						unsigned int dep;
						dep = monTableau.getEdition();
						std::string nom;
						if(pointeur.objectif == ORIGINE) nom = fichier1;
						else if(pointeur.objectif == DESTINATION) nom = fichier2;
						std::cout << "Insertion d'un point a l'indice " << dep << " sur " << nom << "\n";
						monTableau.inserer(dep, pointeur.x, pointeur.y, pointeur.objectif);
					}
					break;

				case 'd':
					//Suppression du point en cours d'edition
					if(pointeur.objectif == ORIGINE || pointeur.objectif == DESTINATION)
					{
						unsigned int dep;
						dep = monTableau.getEdition();
						std::string nom;
						if(pointeur.objectif == ORIGINE) nom = fichier1;
						else if(pointeur.objectif == DESTINATION) nom = fichier2;
						std::cout << "Suppresion du point d'indice " << dep << " sur " << nom << "\n";
						monTableau.supprimer(dep, pointeur.objectif);
					}
					break;

				case 'e':
					//Echange deux points du tableau actif : celui en cours d'edition et celui defini par l'utilisateur
					if(pointeur.objectif == ORIGINE || pointeur.objectif == DESTINATION)
					{
						unsigned int dest;
						unsigned int dep;
						dep = monTableau.getEdition();
						std::string nom;
						if(pointeur.objectif == ORIGINE) nom = fichier1;
						else if(pointeur.objectif == DESTINATION) nom = fichier2;
						std::cout << "Echange de deux points sur le fichier : " << nom << "\n";
						std::cout << "Le point actuellement selectionne est : " << dep << "\n";
						std::cout << "Veuillez entrer l'indice du point a echanger avec le point courant : ";
						std::cin >> dest;
						monTableau.echanger(dep, dest, pointeur.objectif);
					}
					break;

				//Moins
				#ifdef OS_WINDOWS
					case 45:
				#else
					case (char)(1114029):
				#endif
					pointeur.nouveau = false;
					pointeur.actif = false;
					pointeur.selection = false;
					pointeur.i--;
					monTableau.colorer(pointeur.i);
					break;

				//Plus
				#ifdef OS_WINDOWS
					case 43:
				#else
					case (char)(1114027):
				#endif
					pointeur.nouveau = false;
					pointeur.actif = false;
					pointeur.selection = false;
					pointeur.i++;
					monTableau.colorer(pointeur.i);
					break;

				//Touche entrée
				#ifdef OS_WINDOWS
					case 13:
				#else
					case (char)(1048586):
				#endif
					//Rendu
					pointeur.objectif = RENDU;
					continuer = false;
					break;

				//Pour ne pas engendrer de warning à la compilation
				default:
					break;
			}
		}
		//Différentes actions en fonction de ce choisit l'utilisateur
		switch(pointeur.objectif)
		{
			case RENDU:
				if(!continuer)
				{
					//On ferme les deux fenêtres désormais inutiles.
					destroyWindow(titre_O);
					destroyWindow(titre_D);

					for(unsigned int n = 0; n < nb_img_rendu; n++)
					{
						monTableau.rendu(n, nb_img_rendu);
					}
					monTableau.finaliserVideo(nb_img_rendu);
					std::cout << "Appuyez sur une touche pour quitter.";
					waitKey();
				}
				break;

			default:
				monTableau.afficher(ORIGINE, ecran);
					imshow(titre_O, ecran);
				monTableau.afficher(DESTINATION, ecran);
					imshow(titre_D, ecran);
		}

		//Stockage du nouveau point
		if(pointeur.nouveau)
		{
			pointeur.i = monTableau.stocker(pointeur.x, pointeur.y, pointeur.objectif);
			pointeur.nouveau = false;
			pointeur.actif = false;
		}

		//Sélection d'un point
		if(pointeur.selection)
		{
			pointeur.i = monTableau.trouverPlusProche(pointeur.x, pointeur.y, pointeur.objectif);
			pointeur.selection = false;
		}

		//Déplacement d'un point
		if(pointeur.actif)	monTableau.deplacer(pointeur.i, pointeur.x, pointeur.y, pointeur.objectif);
	}
	return 0;
}
