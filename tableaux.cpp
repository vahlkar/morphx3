#include "tableaux.h"

Tableaux::Tableaux(Rect rect, const Mat &origine, const Mat &destination, std::string fichier1, std::string fichier2, unsigned int fps)
{
	//Permet de récupérer le nom de fichier uniquement
	m_fichier1 = extraireNomFichier(fichier1);
	m_fichier2 = extraireNomFichier(fichier2);

	m_rect = rect;
	m_subdiv.initDelaunay(m_rect);
	m_origine = origine.clone();
	m_destination = destination.clone();
	m_ecran = Mat(rect.size(), m_origine.type());
	m_edition = 0;

	m_extension = ".png";

	reinitialiser(ORIGINE);
	reinitialiser(DESTINATION);
	reinitialiser(INTER);

	std::string titre = "morphing/v_" + m_fichier1 + "_" + m_fichier2 + ".avi";

	if(fps < 3) m_fps = 3;
	else m_fps = fps;
	m_video.open(titre, CODEC, m_fps, rect.size());
	if(!m_video.isOpened()) std::cout << "Erreur ouverture fichier video : " << titre << "\n";

	charger(ORIGINE);
	charger(DESTINATION);
}

Tableaux::~Tableaux()
{
	m_video.release();
}

int Tableaux::stocker(float x, float y, Objectif objectif)
{
	//Permet de choisir le tableau sur lequel on travaille
	std::vector<Point2f> *sommets;
	switch(objectif)
	{
		case ORIGINE:
			sommets = &(m_sommets_debut);
			break;
		case DESTINATION:
			sommets = &(m_sommets_fin);
			break;
		case INTER:
			sommets = &(m_sommets_inter);
			break;
		default:
			return 0;
	}

	int indiceSommet = 0;
	//Si le sommet n'est pas présent dans le tableau, on l'ajoute et on récupère le nouvel indice correspondant dans le tableau
	if(!existe(x, y, indiceSommet, objectif))
	{
		Point2f s;
		s.x = x;
		s.y = y;
		(*sommets).push_back(s); //On ajoute le sommet dans le tableau
		indiceSommet = (*sommets).size() - 1; //A vérifier !!!
	}
	m_edition = indiceSommet; //On identifie le dernier point modifié pour l'affichage
	return indiceSommet; //on renvoie la nouvelle taille du tableau de points
}

bool Tableaux::existe(float x, float y, int &indiceSommet, Objectif objectif)
{
	std::vector<Point2f> *sommets;
	switch(objectif)
	{
		case ORIGINE:
			sommets = &(m_sommets_debut);
			break;
		case DESTINATION:
			sommets = &(m_sommets_fin);
			break;
		case INTER:
			sommets = &(m_sommets_inter);
			break;
		default:
			return 0;
	}

	unsigned int i = 0;
	unsigned int taille = (*sommets).size();
	Point2f s;
	bool existe = false;

	//Pour éviter le tableau vide
	if (taille > 0)
	{
		while(i < taille && !existe)
		{
			s = (*sommets)[i];
			existe = egaliteFlottant(s.x, x) && egaliteFlottant(s.y, y); //Pour éviter les doublons de points (dus aux rounding_error)
			i++;
		}
		if(existe) indiceSommet = i - 1; //ATTENTION ICI on retourne i-1 car même si on a trouvé le sommet, on a incrémenté i !!!
	}
	return existe;
}

int Tableaux::trouverPlusProche(float x, float y, Objectif objectif)
{
	std::vector<Point2f> *sommets;
	switch(objectif)
	{
		case ORIGINE:
			sommets = &(m_sommets_debut);
			break;
		case DESTINATION:
			sommets = &(m_sommets_fin);
			break;
		case INTER:
			sommets = &(m_sommets_inter);
			break;
		default:
			return 0;
	}

	unsigned int taille = (*sommets).size();
	unsigned int i = 0;
	Point2f s;
	double distance_min, distance;
	unsigned int indicePlusProche = 0;

	s = (*sommets)[i];
	distance = hypot((x-s.x),(y-s.y)); //hypot : racine de la somme des carrés (hypoténuse) REMARQUE on peut aussi utiliser le carré de la distance
	distance_min = distance;
	i++;

	while(i < taille && distance_min > 0)
	{
		s = (*sommets)[i];
		distance = hypot((x-s.x),(y-s.y)); //hypot : racine de la somme des carrés (hypoténuse)
		if(distance < distance_min)
		{
			distance_min = distance;
			indicePlusProche = i;
		}
		i++;
	}

	return indicePlusProche;
}

void Tableaux::deplacer(unsigned int i, float v, float w, Objectif objectif)
{
	Point2f *s; //Utilisation obligatoire d'un pointeur étant donné qu'on ne connait pas le tableau à modifier
	switch(objectif)
	{
		case ORIGINE:
			s = &(m_sommets_debut[i]);
			break;
		case DESTINATION:
			s = &(m_sommets_fin[i]);
			break;
		case INTER:
			s = &(m_sommets_inter[i]);
			break;
		default:
			return;
	}
	s->x = v;
	s->y = w;
	m_edition = i; //On identifie le dernier point modifié pour l'affichage
}
void Tableaux::deplacer(float x, float y, float v, float w, Objectif objectif)
{
	deplacer(trouverPlusProche(x, y, objectif), v, w, objectif);
}

bool Tableaux::echanger(unsigned int i, unsigned int j, Objectif objectif)
{
	std::vector<Point2f> *sommets;
	switch(objectif)
	{
		case ORIGINE:
			sommets = &(m_sommets_debut);
			break;
		case DESTINATION:
			sommets = &(m_sommets_fin);
			break;
		default:
			return false;
	}

	unsigned int taille = (*sommets).size();
	if (i < taille && j < taille)
	{
		Point2f si = (*sommets)[i]; //On stocke le point i

		//On d�place le point i sur le point j
		(*sommets)[i].x = (*sommets)[j].x;
		(*sommets)[i].y = (*sommets)[j].y;

		//On d�place le point j a l'ancienne position du point i
		(*sommets)[j].x = si.x;
		(*sommets)[j].y = si.y;

		return true;
	}
	else
		return false;
}

void Tableaux::supprimer(unsigned int i, Objectif objectif)
{
	std::vector<Point2f> *sommets;
	switch(objectif)
	{
		case ORIGINE:
			sommets = &(m_sommets_debut);
			break;
		case DESTINATION:
			sommets = &(m_sommets_fin);
			break;
		default:
			return;
	}
	unsigned int taille = (*sommets).size();

	for(unsigned int k = i + 1; k < taille; k++)
	{
		(*sommets)[k-1] = (*sommets)[k]; //On ecrase le sommet precedent avec les coordonnees du sommet suivant : le sommet i est donc perdu.
	}
	//On retire le tout dernier sommet (dedouble) du tableau.
	(*sommets).pop_back();
}

void Tableaux::inserer(unsigned int i, float x, float y, Objectif objectif)
{
	std::vector<Point2f> *sommets;
	switch(objectif)
	{
		case ORIGINE:
			sommets = &(m_sommets_debut);
			break;
		case DESTINATION:
			sommets = &(m_sommets_fin);
			break;
		default:
			return;
	}
	Point2f s;
	s.x = x;
	s.y = y;
	(*sommets).push_back(s); //On ajoute le sommet dans le tableau (sert uniquement a creer une case de plus, l'ajout reel a lieu a la fin).
	unsigned int taille = (*sommets).size(); //La nouvelle taille

	//On descend dans le tableau en faisant remonter tous les sommets d'une case
	for(unsigned int k = taille - 1; k > i; k--)
	{
		(*sommets)[k] = (*sommets)[k-1];
	}
	//On remplace le sommet i (dedouble en i+1) par s
	(*sommets)[i] = s;
}

void Tableaux::copieMaillage(Objectif depart, Objectif fin)
{
	std::vector<Point2f> *sommets_debut;
	std::vector<Point2f> *sommets_fin;
	switch(depart)
	{
		case ORIGINE:
			sommets_debut = &(m_sommets_debut);
			break;
		case DESTINATION:
			sommets_debut = &(m_sommets_fin);
			break;
		case INTER:
			sommets_debut = &(m_sommets_inter);
			break;
		default:
			return;
	}
	switch(fin)
	{
		case ORIGINE:
			sommets_fin = &(m_sommets_debut);
			break;
		case DESTINATION:
			sommets_fin = &(m_sommets_fin);
			break;
		case INTER:
			sommets_fin = &(m_sommets_inter);
			break;
		default:
			return;
	}

	(*sommets_fin).clear();
	unsigned int tailleTableau = (*sommets_debut).size();
	(*sommets_fin).reserve(tailleTableau);

	//Duplication d'un tableau dans l'autre en fonction du choix utilisateur
	for(unsigned int i = 0; i < tailleTableau; i++)
	{
		Point2f s = (*sommets_debut)[i];
		(*sommets_fin).push_back(s);
	}
}

void Tableaux::reinitialiser(Objectif objectif)
{
	std::vector<Point2f> *sommets;
	switch(objectif)
	{
		case ORIGINE:
			sommets = &(m_sommets_debut);
			break;
		case DESTINATION:
			sommets = &(m_sommets_fin);
			break;
		case INTER:
			sommets = &(m_sommets_inter);
			break;
		default:
			m_ecran = Scalar::all(0);
			return;
	}

	(*sommets).clear();

	m_ecran = Scalar::all(0);
	m_edition = 0;

	//Ajout automatique de points sur les bords
	float w = (m_rect.size()).width - 1; //pour que les points soient a l'interieur de l'ecran
	float h = (m_rect.size()).height - 1;

	//Trace les deux bords horizontaux
	for(float i = 1; inegaliteFlottant(i,w); i+= ((w-1)/3))
	{
		stocker(i, 1, objectif);
		stocker(i, h, objectif);
	}
	//Trace les deux bords verticaux
	for(float j = 1; inegaliteFlottant(j,h); j+= ((h-1)/3))
	{
		stocker(1, j, objectif);
		stocker(w, j, objectif);
	}
}

void Tableaux::actualiserInter(float k)
{
	m_sommets_inter.clear();
	unsigned int tailleTableau = m_sommets_debut.size();

	if (tailleTableau != m_sommets_fin.size()) return; //Au cas ou les deux tableaux ne sont pas de même dimension

	m_sommets_inter.reserve(tailleTableau);

	//Remplissage du tableau.
	for(unsigned int i = 0; i < tailleTableau; i++)
	{
		Point2f d = m_sommets_debut[i];
		Point2f f = m_sommets_fin[i];
		Point2f s;
		s.x = d.x + k*(f.x-d.x);
		s.y = d.y + k*(f.y-d.y);

		m_sommets_inter.push_back(s);
	}

	#if 0
		getSommets(INTER);
		sauvegarder(INTER);
	#endif // 0
}

void Tableaux::triangulation()
{
	m_subdiv.initDelaunay(m_rect);
	unsigned int taille = m_sommets_inter.size();
	for(unsigned int i = 0; i < taille; i++)
	{
		Point2f p = m_sommets_inter[i];
		if(m_rect.contains(p))
			m_subdiv.insert(p);
	}
}

unsigned int Tableaux::actualiserTriangles()
{
	m_triangles.clear();

	std::vector<Vec6f> triangleList;
    m_subdiv.getTriangleList(triangleList);
    unsigned int taille = triangleList.size();

    std::vector<Point2f> pt(3);
	std::array<int,3> triangle;

    for(unsigned int i = 0; i < taille; i++)
    {
		Vec6f t = triangleList[i];
		pt[0] = Point2f(t[0], t[1]);
		pt[1] = Point2f(t[2], t[3]);
		pt[2] = Point2f(t[4], t[5]);

		bool ajouter = true;

		for(int k = 0; k < 3; k++)
		{
			int indiceSommet;
			//Si le sommet n'est pas présent dans le tableau, on ne tient pas compte du triangle (points en dehors de l'écran générés par OpenCV)
			ajouter = ajouter && existe(pt[k].x, pt[k].y, indiceSommet, INTER);
			triangle[k] = indiceSommet;
		}
		if(ajouter) m_triangles.push_back(triangle);
	}
	std::cout << taille << " triangles ont ete generes.\n";
	std::cout << m_triangles.size() << " triangles ont ete ajoutes au tableau.\n";
	return m_triangles.size();
}

bool Tableaux::appartenance(unsigned int iTriangle, Point2f s)
{
	//Méthode des demis-plans
	bool reponse = true;
	if(iTriangle < m_triangles.size())
	{
		std::array<int,3> t = m_triangles[iTriangle];
		int i = 0;
		while(i < 3 && reponse)
		{
			float a = 0, b = 0;
			//Définition des deux sommets du triangle opposés au sommet actuellement traité
			int iv1 = ((i + 1)%3), iv2 = ((i+2)%3);
			Point2f sv0 = m_sommets_inter[t[i]], sv1 = m_sommets_inter[t[iv1]], sv2 = m_sommets_inter[t[iv2]];
			//Calcul de l'équation de la droite x=a ou y = ax+b;
			if(egaliteFlottant(sv1.x, sv2.x))
			{
				//Cas x=a
				a = sv1.x;

				//Si les deux points (le point testé et le troisième sommet) sont du même côté du demi-plan
				reponse = reponse && (inegaliteFlottant(a, sv0.x) == inegaliteFlottant(a, s.x));
			}
			else
			{
				//Cas y = ax+b;
				a = (sv1.y - sv2.y)/(sv1.x - sv2.x);
				b = sv1.y - a*(sv1.x);

				//Si les deux points (le point testé et le troisième sommet) sont du même côté du demi-plan
				reponse = reponse && (inegaliteFlottant(sv0.y, a*(sv0.x)+b) == inegaliteFlottant(s.y, a*(s.x)+b));
			}
			i++;
		}
	}
	return reponse;
}

unsigned int Tableaux::trouverTriangle(Point2f s)
{
	unsigned int tailleTriangles = m_triangles.size();
	unsigned int i = 0;
	bool appartient = false;

	while(i < tailleTriangles && !appartient)
	{
		appartient = appartenance(i,s);
		i++;
	}

	if(appartient)
		return (i-1); //On a incrémenté en fin de boucle donc on retourne (i-1)
	else
		return 0;
}

void Tableaux::transformer(Point2f s, Vec3b &c, Objectif objectif)
{
	Mat *image;
	unsigned int indiceTriangle = trouverTriangle(s);

	std::vector<Point2f> *sommets;
	switch(objectif)
	{
		case ORIGINE:
			sommets = &(m_sommets_debut);
			image = &(m_origine);
			break;
		case DESTINATION:
			sommets = &(m_sommets_fin);
			image = &(m_destination);
			break;
		case INTER:
			return;
			break;
		default:
			return;
	}

	//Afin d'être certain que le sommet s appartient bien à un triangle.
	if(appartenance(indiceTriangle, s))
	{
		std::array<int,3> triangle = m_triangles[indiceTriangle];
		Point2f f0 = m_sommets_inter[triangle[0]];
		Point2f f01 = m_sommets_inter[triangle[1]]; //Vecteur F0F1
			f01.x -= f0.x;
			f01.y -= f0.y;
		Point2f f02 = m_sommets_inter[triangle[2]];
			f02.x -= f0.x;
			f02.y -= f0.y;

		Point2f d0 = (*sommets)[triangle[0]];
		Point2f d01 = (*sommets)[triangle[1]];
			d01.x -= d0.x;
			d01.y -= d0.y;
		Point2f d02 = (*sommets)[triangle[2]];
			d02.x -= d0.x;
			d02.y -= d0.y;

		Point2f differentiel; //Vecteur position dans la base cannonique

		differentiel.x = s.x - f0.x;
		differentiel.y = s.y - f0.y;

		Point2f destination;

		float determinant = f01.x*f02.y-f01.y*f02.x; //ad-bc
		if(!egaliteFlottant(determinant,0.))
		{
			//Récupération des coordonnées du vecteur position dans la base triangulaire ((f1-f0),(f2-f0))
			//Produit matriciel : (dx-cy)/determinant ; (-bx + ay)/determinant
			destination.x = (f02.y*(differentiel.x) - f02.x*(differentiel.y))/determinant;
			destination.y = (-f01.y*(differentiel.x) + f01.x*(differentiel.y))/determinant;

			//Pondération, à l'aide de ces mêmes coordonnées, du couple de vecteur ((d1-d0),(d2-d0))
			differentiel.x = destination.x*d01.x + destination.y*d02.x;
			differentiel.y = destination.x*d01.y + destination.y*d02.y;

			//Ajout des coordonnées de d0 pour obtenir la position de s dans le triangle de départ

			destination.x = differentiel.x + d0.x;
			destination.y = differentiel.y + d0.y;

			//Récupération des composantes de couleur
			if(m_rect.contains(destination))
			{
				Vec3b *intensity = (*image).ptr<Vec3b>(destination.y, destination.x); //ATTENTION ! : y avant x !
					c.val[0] = (*intensity).val[0];
					c.val[1] = (*intensity).val[1];
					c.val[2] = (*intensity).val[2];
			}
			else
			{
				#if 1
				Vec3b *intensity = (*image).ptr<Vec3b>(s.y, s.x);
				c.val[0] = (*intensity).val[0];
				c.val[1] = (*intensity).val[1];
				c.val[2] = (*intensity).val[2];
				#else
				c.val[0] = 255;
				c.val[1] = 0;
				c.val[2] = 0;
				#endif // 0
			}
			//cout << ": " << (int)(*intensity).val[0] << " : " << (int)(*intensity).val[1] << " : " << (int)(*intensity).val[2] << "\n";
		}
		else
		{
			//Si le triangle intermédiaire est plat, alors on ne modifie pas le sommet passé en paramètre, mais on extrait néanmoins les composantes de couleur du pixel non transformé
			#if 1
			Vec3b *intensity = (*image).ptr<Vec3b>(s.y, s.x);
				c.val[0] = (*intensity).val[0];
				c.val[1] = (*intensity).val[1];
				c.val[2] = (*intensity).val[2];
			#else
			c.val[0] = 0;
			c.val[1] = 255;
			c.val[2] = 0;
			#endif // 0
		}
	}
	else
	{
		//Si le sommet n'appartient à aucun triangle, alors on ne modifie pas le sommet passé en paramètre, mais on extrait néanmoins les composantes de couleur du pixel non transformé
		#if 1
		Vec3b *intensity = (*image).ptr<Vec3b>(s.y, s.x);
			c.val[0] = (*intensity).val[0];
			c.val[1] = (*intensity).val[1];
			c.val[2] = (*intensity).val[2];
		#else
		c.val[0] = 0;
		c.val[1] = 0;
		c.val[2] = 255;
		#endif // 0
	}
}

void Tableaux::genererCible(float k)
{
    int nmax = 101; //Nombre d'image g�n�r� impair pour avoir une image 50%
    int n = (int)(k*(nmax - 1));

    actualiserInter(k); //Génération de l'ensemble des points intermédiaire
	triangulation(); //Triangulation de cet ensemble

    exporterPoints(0, ORIGINE);
    exporterPoints(0, DESTINATION);
    exporterPoints(n, INTER);
    exporterTriangulation(n); //Sauvegarde triangulation dans image

    rendu(n,nmax - 1); //Indice envoyé correct, seulement la fonction affichera comme texte "image numero n+1" (on commence a 0 dans l'usage classique de rendu())

    std::string fileName;
	std::string progression = messageProgression(n);
    fileName = "mlg_" + m_fichier1 + "_" + m_fichier2 + "_" + progression;

    sauvegarder(INTER, fileName);
}

void Tableaux::rendu(unsigned int n, unsigned int nb_img_max)
{
	//On va de 0 à n-1 -> on a bien n images generees (essentiel de commencer à 0 : avancement est dans [0,1]).
	float k = ((float)n)/((float)(nb_img_max - 1));

	std::cout << "Sauvegarde des maillages...\n";
	sauvegarder(ORIGINE);
	sauvegarder(DESTINATION);

	if (m_sommets_debut.size() != m_sommets_fin.size())
	{
		std::cout << "Erreur : Maillages de tailles differentes !\n";
		std::cout << "Nombre total de sommets " << m_fichier1 << " : " << m_sommets_debut.size() << "\n";
		std::cout << "Nombre total de sommets " << m_fichier2 << " : " << m_sommets_fin.size() << "\n";
		return;
	}

	actualiserInter(k); //Génération de l'ensemble des points intermédiaire
	triangulation(); //Triangulation de cet ensemble

	//Stockage des triangles générés par OpenCV dans le tableau m_triangles
	if(actualiserTriangles() == 0)	return;

	Point2f pt;

	std::cout << "Rendu en cours...\n";

	//Duplication de l'image d'origine dans l'image de rendu
	Mat img_rendu = Mat(m_rect.size(), m_origine.type());
	Mat img_renduO = Mat(m_rect.size(), m_origine.type());
	Mat img_renduD = Mat(m_rect.size(), m_origine.type());
	int jmax = m_rect.size().height;
	int imax = m_rect.size().width;

	std::cout << "Largeur : " << imax << "\n";
	std::cout << "Hauteur : " << jmax << "\n";

    for (int j = 0; j < jmax; j++)
    {
        float charge = ((float)j/(float)jmax)*100;
        //On affiche uniquement les pourcentages entiers
        if(charge == (float)((int)charge)) std::cout << charge << " % (" << (n+1) << " / " << nb_img_max << ")\n"; //Affichage de 1 à nb_max

        for (int i = 0; i < imax; i++)
        {
			Vec3b sO;
			Vec3b sD;
			pt.x = i;
			pt.y = j;

			transformer(pt, sO, ORIGINE);
			transformer(pt, sD, DESTINATION);

			//On stocke la nouvelle couleur ds l'image
			Vec3b *intensityO = img_renduO.ptr<Vec3b>(j, i); //ATTENTION ! : j avant i !
			for(int k = 0; k < 3; k++) (*intensityO).val[k] = sO.val[k];

			Vec3b *intensityD = img_renduD.ptr<Vec3b>(j, i);
			for(int k = 0; k < 3; k++) (*intensityD).val[k] = sD.val[k];

			//Mélange des deux images ainsi générées, couleurs mélangées en fonction de k
			Vec3b s;

			s.val[0] = (sO.val[0] + k*(sD.val[0] - sO.val[0]));
			s.val[1] = (sO.val[1] + k*(sD.val[1] - sO.val[1]));
			s.val[2] = (sO.val[2] + k*(sD.val[2] - sO.val[2]));
			//cout << "; " << (int)s.val[0] << " : " << (int)s.val[1] << " : " << (int)s.val[2] << "\n";

			//On stocke la nouvelle couleur ds l'image
			Vec3b *intensity = img_rendu.ptr<Vec3b>(j, i);
			for(int k = 0; k < 3; k++) (*intensity).val[k] = s.val[k];
        }
    }
	std::cout << 100 << " %\n";
	std::cout << "Rendu termine.\n";

    exporterImage(img_rendu,n,"mlg","morphing/", INTER);
    exporterImage(img_renduO,n,"de","morphing/", ORIGINE);
    exporterImage(img_renduD,n,"de","morphing/", DESTINATION);
}

void Tableaux::finaliserVideo(int nb_img_rendu)
{
    std::cout << "Generation de la video en cours :\n";
    std::cout << "Chargement du morphing...\n";
    //On affiche 2 fois l'image d'indice nb_img_rendu -1 : Pour équilibrer la latence des lecteurs videos lorsque l'on lit la video en boucle
    for(int n = 0; n < 2*nb_img_rendu; n++)
    {
         std::string progression;
         if(n < nb_img_rendu) progression = messageProgression(n);
         else progression = messageProgression(2*nb_img_rendu - 1 - n);

        String fileName = "morphing/mlg_" + m_fichier1 + "_" + m_fichier2 + "_" + progression + m_extension;
        Mat img_rendu = imread(fileName);

        if(m_video.isOpened())
        {
            std::cout << "Ajout de l'image " << fileName << " a la video.\n";
            m_video.write(img_rendu);
        }
        else
            std::cout << "Echec : fichier video non accessible.\n";
    }
    std::cout << "Nombre d'images : 2*" << nb_img_rendu << "\n";
    std::cout << "Nombre d'images par secondes : " << m_fps << "\n";
    std::cout << "Duree de la video : " << ((float)(2*nb_img_rendu))/((float)m_fps) << " s\n";
    std::cout << "Generation de la video terminee.\n";
}

void Tableaux::colorer(int i)
{
	unsigned int taille = std::max(m_sommets_debut.size(), m_sommets_fin.size());
	m_edition = i % taille;
}

void Tableaux::afficher(Objectif objectif, Mat &ecran)
{
	std::vector<Point2f> *sommets;
	switch(objectif)
	{
		case ORIGINE:
			sommets = &(m_sommets_debut);
			m_ecran = m_origine.clone();
			break;
		case DESTINATION:
			sommets = &(m_sommets_fin);
			m_ecran = m_destination.clone();
			break;
		case INTER:
			sommets = &(m_sommets_inter);
			m_ecran = Scalar::all(0);
			break;
		default:
			return;
	}

	unsigned int taille = (*sommets).size();
	for(unsigned int i = 0; i < taille; i++)
	{
		if(m_edition == i)
			circle(m_ecran, (*sommets)[i], TAILLE_POINTS, Scalar(0,0,255),-1);
		else
			circle(m_ecran, (*sommets)[i], TAILLE_POINTS, Scalar(180,0,0),-1);
	}
	ecran = m_ecran;
}

void Tableaux::exporterPoints(unsigned int n, Objectif objectif)
{
    unsigned int taille_points = 2;
    Mat points;
    Scalar delaunay_color(180,0,0); //Couleur bleue des points
    std::vector<Point2f> *sommets;
	switch(objectif)
	{
		case ORIGINE:
			sommets = &(m_sommets_debut);
			points = m_origine.clone();
			break;
		case DESTINATION:
			sommets = &(m_sommets_fin);
			points = m_destination.clone();
			break;
		case INTER:
			sommets = &(m_sommets_inter);
			points = Mat(m_rect.size(), m_origine.type());
			points = Scalar::all(255);
			taille_points = 3; //On grossit les points pour l'exportation des points seuls
			break;
		default:
			return;
	}

	unsigned int taille = (*sommets).size();
	for(unsigned int i = 0; i < taille; i++)
	{
		if(m_edition == i)
			circle(points, (*sommets)[i], taille_points, Scalar(0,0,255),-1);
		else
			circle(points, (*sommets)[i], taille_points, Scalar(180,0,0),-1);
	}
	exporterImage(points, n, "pts","triangulation/",objectif);
}
void Tableaux::exporterTriangulation(unsigned int n)
{
	Mat triangulation = Mat(m_rect.size(), m_origine.type());
	triangulation = Scalar::all(0);
	Scalar delaunay_color(255,255,255);

	std::vector<Vec6f> triangleList;
    m_subdiv.getTriangleList(triangleList);
    std::vector<Point> pt(3);

    for( size_t i = 0; i < triangleList.size(); i++ )
    {
        Vec6f t = triangleList[i];
        pt[0] = Point(cvRound(t[0]), cvRound(t[1]));
        pt[1] = Point(cvRound(t[2]), cvRound(t[3]));
        pt[2] = Point(cvRound(t[4]), cvRound(t[5]));
        line(triangulation, pt[0], pt[1], delaunay_color);
        line(triangulation, pt[1], pt[2], delaunay_color);
        line(triangulation, pt[2], pt[0], delaunay_color);
    }
    exporterImage(triangulation,n,"tr","triangulation/", INTER);
}
void Tableaux::exporterImage(Mat &image, unsigned int n, std::string prefixe, std::string repertoire, Objectif objectif)
{
    std::string fichier;
    switch(objectif)
	{
		case ORIGINE:
			fichier = m_fichier1;
			break;
		case DESTINATION:
			fichier = m_fichier2;
			break;
		case INTER:
			fichier = m_fichier1 + "_" + m_fichier2;
			break;
		default:
			return;
	}

    std::vector<int> compression_params;
    compression_params.push_back(IMWRITE_PNG_COMPRESSION);
   	compression_params.push_back(9);
   	m_extension = ".png";

	//Sauvegarde des images dans des fichiers .png
	std::string fileName;
	std::string progression = messageProgression(n);

    fileName = repertoire + prefixe + "_" + fichier + "_" + progression + m_extension;
    std::cout << "Sauvegarde image : " << fileName << "\n";
    imwrite(fileName, image, compression_params);
}

void Tableaux::charger(Objectif objectif)
{
	std::string fileName;
	fileName = "sauvegardes/ms_";
	reinitialiser(objectif); //On vide le tableau avant de le charger

	switch(objectif)
	{
		case ORIGINE:
			fileName += m_fichier1;
			break;
		case DESTINATION:
			fileName += m_fichier2;
			break;
		default:
			return;
	}
	fileName += ".morphx3";

	//Ouverture du fichier
	std::ifstream lecture(fileName.c_str());
	if(!lecture)
		std::cout << "ERREUR: Impossible d'ouvrir le fichier en lecture :" << fileName << "\n";
	else
	{
		std::string ligne; //Stocke les lignes lues
      	while(getline(lecture, ligne)) //Tant qu'on n'est pas à la fin, on lit
      	{
			if(!ligne.empty())
			{
				unsigned int debut = ligne.find("(");
				unsigned int inter,fin;
				if(debut != std::string::npos) //Si on trouve une parenthèse ouvrante
				{
					debut = ligne.find("(");
					inter = ligne.find(",");
					fin = ligne.find(")");

					if(debut != std::string::npos && inter != std::string::npos && fin != std::string::npos)
					{
						Point2f s;
						std::string x = ligne.substr(debut + 1, inter - 1); //On ne prend pas les parenthèses
						std::string y = ligne.substr(inter + 1, fin - 1);
						std::stringstream ss;
						ss << x;
							ss >> (s.x);
						ss.clear();
						ss << y;
							ss >> (s.y);

						stocker(s.x, s.y, objectif);
					}
				}
			}
      	}
      	std::cout << "Chargement effectue : " << fileName << "\n";
	}
}

void Tableaux::sauvegarder(Objectif objectif, std::string inter)
{
	std::string fileName;
	fileName = "sauvegardes/ms_";

	std::vector<Point2f> *sommets;
	switch(objectif)
	{
		case ORIGINE:
			sommets = &(m_sommets_debut);
			fileName += m_fichier1;
			break;
		case DESTINATION:
			sommets = &(m_sommets_fin);
			fileName += m_fichier2;
			break;
		case INTER:
			sommets = &(m_sommets_inter);
			fileName += inter;
			break;
		default:
			return;
	}
	fileName += ".morphx3";

	std::ofstream sortie(fileName.c_str());
	if(!sortie)
		std::cout << "ERREUR: Impossible d'ouvrir le fichier en écriture :" << fileName << "\n";
	else
	{
		sortie << "//------------------Sauvegarde maillage MorpHX3------------------\\\n";

		unsigned int taille = (*sommets).size();
		sortie << "Nombre total de sommets : " << taille << "\n";

		sortie << "BOF\n"; //Beginning of file
		for (unsigned int i = 0; i < taille; i++)
		{
			sortie << "(" << (*sommets)[i].x << "," << (*sommets)[i].y << ")\n";
		}
		sortie << "EOF";

		std::cout << "Sauvegarde effectue : " << fileName << "\n";
	}
}

const void Tableaux::getSommets(Objectif objectif)
{
	std::string message = "m_sommets_";
	std::vector<Point2f> *sommets;
	switch(objectif)
	{
		case ORIGINE:
			sommets = &(m_sommets_debut);
			message += "debut";
			break;
		case DESTINATION:
			sommets = &(m_sommets_fin);
			message += "fin";
			break;
		case INTER:
			sommets = &(m_sommets_inter);
			message += "inter";
			break;
		default:
			return;
	}

	unsigned int taille = (*sommets).size();

	std::cout << "\n-----Affichage tableau de sommets : " << message << "-----\n";
	std::cout << "| i | X | Y |\n";
	for (unsigned int i = 0; i < taille; i++)
	{
		std::cout << "| " << i << " | " << (*sommets)[i].x << " | " << (*sommets)[i].y << " |\n";
	}
	std::cout << "Nombre total de sommets : " << taille << "\nNombre limite de sommets : " << (*sommets).capacity() << "\n";
	std::cout << "\n-----Fin d'affichage tableau de sommets : " << message << "-----\n";
}

const void Tableaux::getTriangles()
{
	unsigned int taille = m_triangles.size();
	std::cout << "\nAppel de getTriangles() :\n";
	std::cout << "Nombre total de triangles : " << taille << " ; Nombre limite de triangles (système) : " << m_triangles.capacity() << "\n";

	std::cout << "\n------------------Affichage tableau de triangles : m_triangles------------------\n";
	std::cout << "| i | X | Y |\n";
	for (unsigned int i = 0; i < taille; i++)
	{
		for(unsigned k = 0; k < 3 ; k++)
		{
			int indiceSommet = m_triangles[i][k];
			std::cout << "| " << indiceSommet << " | " << m_sommets_debut[indiceSommet].x << " | " << m_sommets_debut[indiceSommet].y << " | \n";
		}
		std::cout << "\n";
	}
	std::cout << "\n---------------Fin d'affichage tableau de triangles : m_triangles---------------\n";
}

bool Tableaux::verifierTailles(unsigned int &t1, unsigned int &t2)
{
	t1 = m_sommets_debut.size();
	t2 = m_sommets_fin.size();
	return t1 == t2;
}

unsigned int Tableaux::getEdition()
{
	return m_edition;
}
