#include <iostream>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <string>
#include <cmath>

void help();
std::string messageProgression(int n);
std::string extraireNomFichier(std::string chemin);
bool egaliteFlottant(float a, float b, int precision = -5);
bool inegaliteFlottant(float a, float b, int precision = -5);
