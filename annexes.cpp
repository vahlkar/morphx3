#include "annexes.h"

void help()
{
    std::cout << "\nMorpHX3 Commandes :\n"
            "h: Afficher ce message d'aide.\n"
            "t: Affichage du nombre de sommets places sur chaque image.\n"
            "r: Supprime tous les sommets de l'image en cours d'edition.\n"
            "g: Affiche le tableau de sommets de l'image en cours a l'ecran.\n"
            "s: Sauvegarde le maillage actuel.\n"
            "c: Charge le maillage precedemment sauvegarde. Attention efface sans \nconfirmation le maillage en cours d'edition !\n"
            "v: Remplace le maillage en cours d'edition par celui de l'autre image.\n"
            "+ ou - : Colorer le point suivant/precedent.\n"
            "m: Genere la video a l'aide des images generees precedemment par le rendu.\n"
            "w: Afficher l'indice du point en cours d'edition (rouge).\n"
            "p: Inserer un point a la position actuelle (rouge) dans le tableau actif.\n"
            "d: Supprimer du tableau actif le point en cours d'edition.\n(La souris doit etre a l'interieur de la bonne fenetre !)\n"
            "e: Echanger le point en cours d'edition avec le point d'indice i \n(defini par l'utilisateur).\n"
            "MAJUSCULE, en meme temps que le clic gauche de souris permet de \ndeplacer un point.\n"
            "ENTREE: Generer le rendu (morphing des deux images).\n"
            "i: Genere l'image cible et sauvegarde le maillage associe.\n"
            "ECHAP: Quitter le programme.\n"
            "Remarque : Toutes ces touches doivent etre saisies avec la souris a \nl'interieur d'une des fenetres d'edition des images.\n\n";
}

bool egaliteFlottant(float a, float b, int precision)
{
	if(a != b)
	{
		int ldiff = (int)(std::log10(std::abs(a-b)));
		return (ldiff <= precision);
	}
	else return true;
}

bool inegaliteFlottant(float a, float b, int precision) //a<=b
{
	return (egaliteFlottant(a, b, precision) || a<=b); //Si a et b ne sont pas égaux, alors ils sont suffisament éloignés pour utiliser l'opérateur classique de comparaison sur les flottants)
}
/*
REMARQUE :
Pour obtenir a<b : inegaliteFlottant(a,b)&(!egaliteFlottant(a,b))
Pour obtenir a>b : (!inegaliteFlottant(a,b))
Pour obtenir a>=b : (!inegaliteFlottant(a,b))||egaliteFlottant(a,b) ; ou alors inegaliteFlottant(b,a)
*/

std::string messageProgression(int n)
{
	std::string progression;
		std::stringstream *convertI = new std::stringstream;
		*convertI << n;//Pour avoir la structure indiquée par le pointeur
		//Afin d'afficher un nombre sur 3 chiffres (on ne va que de 0 a 100 donc on ajoute 0 au debut sauf si k = 1)
		if (std::log10(n) < 1) progression += "00";
		else if (std::log10(n) < 2) progression += "0";
		progression += convertI->str();
	delete convertI;

	return progression;
}

std::string extraireNomFichier(std::string chemin)
{
	//Permet, a partir du chemin complet d'un fichier, de récupérer le nom de fichier uniquement
	int debut = chemin.rfind("/") + 1;
	int taille = chemin.rfind(".") - debut;
	return chemin.substr(debut, taille);
}

