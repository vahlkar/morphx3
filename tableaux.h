#ifndef MORPHX3_TABLEAUX
#define MORPHX3_TABLEAUX

//#define OS_WINDOWS

#include <iostream>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include <array>
#include <cmath>

#include "annexes.h"

//2 pour utilisation normale ; 3 pour mettre en valeur les points
#define TAILLE_POINTS 2

//Image par secondes dans la sauvegarde video
//Attention en fonction du codec utilise, il est possible qu'une valeur de FPS trop basse ne fonctionne pas (essayer 5, 10, 20...)
#define FPS_DEFAUT 5
//Numero du codec utilise -> mettre -1 pour avoir le choix a l'ouverture du programme
#ifdef OS_WINDOWS
	//Windows
	#define CODEC 0
#else
	//Unix machine
	#define CODEC CV_FOURCC('M','J','P','G')
#endif

using namespace cv;

//Permet de préciser lors de l'appel des différentes fonctions ce sur quoi on doit travailler
enum Objectif
{
	ORIGINE, DESTINATION, INTER, RENDU
};

class Tableaux
{
		public:
			Tableaux(Rect rect, const Mat &origine, const Mat &destination, std::string fichier1, std::string fichier2, unsigned int fps);
			~Tableaux();

			int stocker(float x, float y, Objectif objectif); //Ajoute un nouveau point au tableau
			bool existe(float x, float y, int &indiceSommet, Objectif objectif); //Vérifie si le point existe dans le tableau de sommet, si c'est le cas son indice correspondant est stocké dans indiceSommet.
			int trouverPlusProche(float x, float y, Objectif objectif); //Trouve le point du tableau le plus proche du point de coordonnées (x,y)

			void deplacer(unsigned int i, float v, float w, Objectif objectif); //Si l'on connait directement le rang d'un point dans le tableau -> plus rapide

			void deplacer(float x, float y, float v, float w, Objectif objectif); //Remplace le couple (x,y), ou le point le plus proche du couple (x,y) par le couple (v,w) ; les couleurs ne changent pas (principe du d�placement).
			void colorer(int i); //Choix du point en surbrillance

			bool echanger(unsigned int i, unsigned int j, Objectif objectif); //Permet d'�changer deux sommets dans le tableau correspondant
			void supprimer(unsigned int i, Objectif objectif); //Permet de supprimer le ieme sommet du tableau correspondant
			void inserer(unsigned int i, float x, float y, Objectif objectif); //Permet d'ins�rer un sommet dans la ieme case du tableau correspondant : d�cale de 1 les points d'indice >= i.

			void copieMaillage(Objectif depart, Objectif arrivee); //Duplique le maillage "depart" dans le maillage "fin"
			void reinitialiser(Objectif objectif); //Vide entièrement chaque tableau

			void rendu(unsigned int n, unsigned int nb_img_max); //Génère le rendu en fonction du coefficient k ; retourne le pointeur sur la surface créée ; Sauvegarde les images générées dans des fichiers
            void finaliserVideo(int nb_img_rendu); //Genere la video en utilisant les images generees par la fonction rendu() (Peut etre utilisee avec des images generees precedemment : non necessaire de relancer le rendu à chaque fois)

            void genererCible(float k);

			void afficher(Objectif objectif, Mat &ecran); //Affiche à l'écran les points sous la forme passée en paramètre

			//Fonctions d'archivage du maillage
			void charger(Objectif objectif); //Charge le maillage défini par objectif
			void sauvegarder(Objectif objectif, std::string inter = "inter"); //Sauvegarde le maillage défini par objectif

			const void getSommets(Objectif objectif); //Affiche le contenu des tableaux
			const void getTriangles();
			bool verifierTailles(unsigned int &t1, unsigned int &t2);

			unsigned int getEdition(); //Retourne l'indice du point en cours d'�dition

		private:
			void exporterPoints(unsigned int n, Objectif objectif);
			void exporterTriangulation(unsigned int n);

			void exporterImage(Mat &image, unsigned int n, std::string prefixe, std::string repertoire, Objectif objectif);

			void actualiserInter(float k = 0.5); //Met à jour les coordonnées des sommets du tableau m_sommets_inter en faisant la moyenne pondérée des coordonnées des sommets de m_sommets_debut et m_sommets_fin
			void triangulation(); //Triangule l'ensemble de points m_inter selon le critère de Delaunay
			unsigned int actualiserTriangles(); //On stocke les triangles générés pas la triangulation dans une structure permettant de mieux les exploiter.

			bool appartenance(unsigned int iTriangle, Point2f s); //Teste si un sommet s appartient au triangle iTriangle ; On recherche dans les triangles de m_sommets_inter (principe de la transformation inverse)
			unsigned int trouverTriangle(Point2f s); //Retourne l'indice du triangle contenant s

			void transformer(Point2f s, Vec3b &c, Objectif objectif); //Met à jour les composantes couleurs du sommet passé en paramètre en fonction du triangle auquel il appartient

		//Attributs
			Mat m_origine;
			Mat m_destination;
			Mat m_ecran;

			VideoWriter m_video;
			unsigned int m_fps; //Sert uniquement a l'affichage dans finaliserVideo()

			Subdiv2D m_subdiv;
			Rect m_rect;
			std::string m_fichier1;
			std::string m_fichier2;
			std::string m_extension;

			/*Sert à afficher le dernier point modifié d'une couleur différente des autres ;
			Vaut l'indice du point en question.	La mise à jour de la valeur a lieu dans deplacer()*/

			unsigned int m_edition;

			//Les trois tableaux de sommets et le tableau de triangles
			std::vector<Point2f> m_sommets_debut;
			std::vector<Point2f> m_sommets_inter;
			std::vector<Point2f> m_sommets_fin;

			std::vector<std::array<int,3>> m_triangles;
};

#endif  //MORPHX3_TABLEAUX
